import 'package:flutter/material.dart';
  void main(){
    runApp(const cube());
  }
  class cube extends StatelessWidget {
    const cube({Key? key}) : super(key: key);

    @override
    Widget build(BuildContext context) {
      return MaterialApp(
        home: Scaffold(
          backgroundColor: Colors.blue.shade100,
          appBar: AppBar(
             title: const Center( child: Text('First Sreen of My apl', style: TextStyle(color: Colors.black
             )
             ),
            ),
          ),

          body: Padding(
            padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 20),
            child: Row(
              children: [
                Column(
                  children: [
                    Container(
                    height: 75,
                    width: 75,
                    color: Colors.red,
                      child: const Center(child: Text('1', style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      ),
                  ),
                    Container(
                      height: 95,
                      width: 95,
                      color: Colors.yellow,
                      child: const Center(child: Text('2', style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      ),
                    ),
                    Container(
                      height: 115,
                      width: 115,
                      color: Colors.green,
                      child: const Center(child: Text('3', style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      ),
                    )
                  ],
      ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [ Container(
                    height: 75,
                    width: 75,
                    color: Colors.red,
                    child: const Center(child: Text('1', style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    ),
                  ),
                    Container(
                      height: 95,
                      width: 95,
                      color: Colors.yellow,
                      child: const Center(child: Text('2', style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      ),
                    ),
                    Container(
                      height: 115,
                      width: 115,
                      color: Colors.green,
                      child: const Center(child: Text('3', style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      ),
                    )
                  ],
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.end,

                  children: [ const Spacer(), Container(
                    height: 75,
                    width: 75,
                    color: Colors.red,
                    child: const Center(child: Text('1', style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    ),
                  ),
                    Container(
                      height: 95,
                      width: 95,
                      color: Colors.yellow,
                      child: const Center(child: Text('2', style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      ),
                    ),
                    Container(
                      height: 115,
                      width: 115,
                      color: Colors.green,
                      child: const Center(child: Text('3', style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      ),
                    )
                  ],
                ),
              ],
            ),
          ),
        ),
      );
    }
  }
  
